#!/usr/bin/env python3

import yaml
import os

conf_file = "user_conf.yml"
classes = ["OEAIT18EIA", "OEAIT18EIB"]

if __name__ == "__main__":

    with open(conf_file, 'r') as stream:
        cfg = yaml.safe_load(stream)

    try:
        print( "'first_name': First name is a string", str(cfg['first_name'] ) )
        print( "'class_number': Class_number is a string", str(cfg['class_number'] ) )
        if not cfg['class_number'] in classes:
                raise ValueError("class_number '%s' not valid"%cfg['class_number'])


    except Exception as ex:
        print( "Config file error")
        print( "Exception was: ", ex )
        exit(1)

    exit(0)

